#pragma once

#include <iosfwd>

enum class Fruit
{
    kiwi,
    apple
};

std::ostream& operator<<(std::ostream& os, Fruit fruit);
std::istream& operator>>(std::istream& is, Fruit& fruit);